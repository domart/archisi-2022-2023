---
author: Benoît Domart
title: Exercice 3 - Implémenter un WS
---

# Exercice 3 - Implémenter un WS avec JAX-WS

Dans cet exercice, nous allons créer notre propre service. Pour cela, nous allons utiliser l'API JAX-WS.

C'est donc ici la méthode *Contract Last*, puisque nous allons écrire du code Java, et le WSDL va être automatiquement généré.

## Utilisation de JAX-WS

???+success "Cours (JAX-WS)"

    JAX-WS signifie *Java XML Web Service*.

    1. Il suffit en fait de créer une classe dans la partie présentation, sur laquelle on positionne l'annotation

        ```java title=""
        @WebService(serviceName = "<le_nom_du_web_service>")
        ```

    2. Dans cette classe, sur chaque méthode qui va être "exposée", c'est-à-dire qui correspondra à un *end point* du WS, il faut positionner l'annotation

        ```java title=""
        @WebMethod(operationName = "<le_nom_du_end_point>")
        ```

        ???+remarque
            Par défaut, si l'attribut `operartionName` n'est pas renseigné, le nom du *end point* est celui de la méthode Java.

    3. Enfin, il faut préciser les arguments de ces méthodes qui sont des paramètres du WS, en utilisant l'annotation `@WebParam` :

        ```java title=""
        @WebMethod(operationName = "books")
        public List<BookBean> getBooks(@WebParam(name = "criteria") String search) {
            ...
        }
        ```
        
        ???+warning "Attention"
            Il **faut** préciser le `name`. En effet, si celui-ci n'est pas précisé, dans le WSDL, les attributs s’appelleront `arg0`, `arg1`, ... ce qui n'est pas très explicite !

        Dans l'exemple ci-dessus, dans le WSDL, on pourra appeler le *end point* `books`, avec le paramètre `criteria`.
    
    4. Une fois le projet déployé sur le serveur, le WSDL est automatiquement généré ici (c'est le principe du *Contract last*) :

    ``` title=""
    http://localhost:8080/<nom_du_projet>/<le_nom_du_web_service>?wsdl
    ```

???+exercice "Exercice 3.1 - Sécurité sociale"

	1. Créer un Web Service permettant de vérifier qu'un numéro de sécurité sociale est correct[^1], c'est-à-dire que la clef de contrôle (les deux derniers chiffres) est correcte.
	2. Tester ce Web Service avec SoapUI.
	3. Créer un client Java qui consomme ce Web Service.<br>
		Attention, ce client doit lui-même être un (autre) projet Web dynamique. En effet, vous devez créer une page Web contenant un champ où indiquer un numéro de sécurité sociale et un bouton "Valider".<br>
		Lorsque l'utilisateur clique sur "Valider", un message apparaît à côté du bouton "Valider" et indique si le numéro de sécurité sociale est correct ou pas.
    
    [^1]: L'algorithme de vérification d'un numéro de sécurité sociale est détaillé [ici](https://selectra.info/assurance/mutuelle-sante/securite-sociale/cle){target=_blank}.


## Utilisation de JAXB

???+success "Cours (JAXB)"

    Le résultat renvoyé par un WS est souvent composé de plusieurs informations, et pas uniquement d'une chaîne de caractère (comme pour la capitale) ou d'un booléen (comme dans l'exercice précédent).

    On souhaite souvent renvoyer l'équivalent d'un *bean* ou d'une liste de *beans*.

    Pour cela, on utilise *JAXB*, pour *Java Architecture XML Binding*. Cette API permet de créer des schémas à partir de classe, et inversement.

    Dès qu'un *bean* est renvoyé par un WS généré avec JAX-WS, JAXB est **automatiquement** utilisé. L'utilisation de JAXB se fait via des annotations à placer sur le *bean* concerné.

    <center>
        ![Exemple JAXB](../images/jaxb-exemple.png)<br/>
        *Un exemple trouvé sur [Développons en Java](https://www.jmdoudoux.fr/java/dej/chap-jaxb.htm#jaxb-2-6-3)*{target=_blank}
    </center>

    Voici quelques annotations que nous pourrions être amenés à utiliser :

    - `XmlRootElement` : Indique que ce *bean* sera mappé avec JAXB.

        ???+remarque
            Non obligatoire avec JAX-WS : dès qu'on utilise cette API, on utilise nécessairement JAXB en même temps.

    - `@XmlTransient`: Indique que l'attribut ne doit pas être sérialisé (il n'apparaît pas dans le fichier XML).

        ???+warning "Attention"
            Cette annotation doit être placée sur un attribut ou une méthode public.

            Il ne faut donc pas la placer sur l'attribut directement (celui-ci doit normalement être `private`).

            Pour JAXB, le `getter` et les `setter` sont reliés. Il suffit donc de la placer uniquement sur le `getter`.
    - `@XmlElement` : Convertir une variable en un élément XML.
    - `@XmlAttribute` : Convertir une variable en un attribut XML.

    Il y en a de nombreuses autres.


???+exercice "Exercice 3.2 - Retour sur les notes"

    Reprenez le projet ***GestionNotes*** des TD6 et TD7, et exposez les deux services suivants :
    
    - récupération de toutes les notes
    - insertion d'une nouvelle note.

    :warning: L'id ne doit pas être exposé.

___

???+exercice "Exercice 3.3 - Remboursement d'un prêt"

	1. Créer un Web Service qui permet de déterminer le montant de la mensualité d'un crédit en fonction du montant emprunté, de la durée de remboursement et du taux d'intérêt annuel
	
		On pourra s'aider de la formule suivante :
		
		\[\text{mensualité} = \dfrac{\text{capital}\times{}\text{tauxmensuel}}{1-\left(1+\text{tauxmensuel}\right)^{-\text{nbMensualités}}}\]
        
        où :
        
        - \(\text{capital}\) correspond au montant emprunté,
        - \(\text{nbMensualités}\) correspond au nombre de mensualités (par exemple, 240 pour 20 ans),
        - et où le taux mensuel est calculé à partir du taux annuel (celui indiqué par la banque ou l'organisme de prêt) avec cette formule :
        
            \[\text{tauxmensuel} = \left(1+\text{tauxannuel}\right)^{\frac{1}{12}}-1\]
        
	2. Tester ce Web Service avec SoapUI.
	3. Créer un client Java, cette fois-ci un projet Java classique, permettant de tester ce web service.
