# TD6 - Connexion à une BDD via JPA 🗃️

L'objectif de ce TD est de reprendre l'application créée dans le TD précédent, mais d'utiliser une API pour implémenter la couche d'accès aux données.

Tout va être automatisé, et nous n'aurons alors plus à effectuer nous même le _mapping_ entre les objets Java et la base de données.

Concrétement, nous allons mettre en place une architecture de ce type :
<center>![archi td5](../images/archi-3tiers.png)</center>