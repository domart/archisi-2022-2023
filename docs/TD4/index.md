# TD4 - Première application Web 💰

L'objectif de ce TD est de créer une première application Web respectant le paradigme MVC (Modèle Vue Contrôleur).

Concrétement, nous allons mettre en place une architecture de ce type :
<center>![archi td4](../images/archi-2tiers-4.png)</center>