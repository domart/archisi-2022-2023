---
author: Benoît Domart
title: Température
---

# TP noté - Ajout de la température

L'objectif de ce TP est de mettre une évolution dans l'application de *location* sur laquelle nous avons travaillé dans le TD précédent.

L'idée est d'afficher la température actuelle (au moment de la recherche) au lieu où se trouve la location.
Dans un premier temps, il est demandé d'afficher cette température dans les résultats affiché par l'API Web développée dans le TD précédent.
S'il vous reste du temps, vous afficherez également cette information dans la page de recherche ([http://localhost:8080/LocationApp/](http://localhost:8080/LocationApp/){target=_blank}).

Pour cela, nous allons utiliser deux API Web :

1. L'API *adresse* : [https://adresse.data.gouv.fr/api-doc/adresse](https://adresse.data.gouv.fr/api-doc/adresse){target=_blank}

    Nous effectuerons la recherche en nous basant sur les champs `address` et `zipCode` du *bean* `LocationBean`, séparés par un espace.
    Cette API permet notamment de récupérer la longitude (champ `0` de `coordinate`) et la latitude (champ `1` de ce même `coordinate`).

2. L'API *Openweathermap* : [https://openweathermap.org/current](https://openweathermap.org/current){target=_blank}

    Elle permet de récupérer la température en kelvin (:warning:) étant données la longitude et la latitude.

???+note "L'API Web *Openweathermap*"

    Pour utiliser l'API *Openweathermap*, il faut une clef.

    Pour cela :

    1. Vous devez vous créer un compte ici : [https://home.openweathermap.org/users/sign_up](https://home.openweathermap.org/users/sign_up){target=_blank}.
    2. Vous pouvez ensuite récupérer votre clef ici : [https://home.openweathermap.org/api_keys](https://home.openweathermap.org/api_keys){target=_blank}.

    Cette clef sera ajoutée en paramètre de requête (*query parameter*) à chaque requête.

    Pour utiliser cette API Web, vous devrez ajouter trois paramètres de requêtes (dans n'importe quel ordre) :

    1. `appid` : La clef qui a été générée pour vous.
    2. `lat` : La latitude de l'endroit où on souhaite connaître la température actuelle.
    3. `lon` : La longitude de l'endroit où on souhaite connaître la température actuelle.


???success "Conseils"

    - Une façon de faire peut être d'ajouter un attribut `temperature` au *bean* `LocationBean`.

        :warning: Attention, ce champ ne doit pas être stocké en BDD, en effet, sa valeur change à chaque instant.
    - Ce champ pourra être mis à jour à chaque récupération de locations en base (c'est-à-dire lors de la récupération de toutes les locations, ou lors de la récupération d'une location via son *id*).
    - Il faudra certainement créer des *DAO* pour appeler les deux API Web utilisées, mais les 3 *DAO* qui existeront alors pourront être appelés depuis un seul *EJB* : `LocationBusinessImpl`.

        En effet, il n'y a pas de raison de créer d'autres classes dans la couche métier. Fonctionnellement, tout ce que nous faisons concerne les locations (et donc `LocationBusinessImpl`).
    - Attention, si aucune adresse n'était renvoyée par l'API Web *adresse*, il ne faudrait pas qu'une exception Java (`NullPointerException` par exemple) ne soit levée.

        Dans ce cas là, la température devrait être à `Null`.
    

    Il serait préférable que la clef de l'API Web *Openweathermap* utilisée soit stockée dans un fichier de configuration (et pas dans une classe Java).

    Il serait également préférable sur les températures affichées soient arrondies au degré.
    