---
author: Benoît Domart
title: Exercice 4 - Implémentation du projet d'entreprise
---

# Exercice 4 - Implémentation du projet d'entreprise

Nous allons maintenant implémenter le cadre contenant les parties "Couche Présentation" et "Couche Métier" du schéma suivant :

<figure markdown>
  ![archi appli](../images/schema_archi_V3.png)
  <figcaption>Architecture de notre application</figcaption>
</figure>

## 1. Création du projet d'entreprise (EAR)

Nous avons créé un module _Web_ et un module _EJB_ qui font partie du même projet. Pour les déployer ensemble sur le serveur, il faut créer un projet _EAR_.

??? note

    Pour être déployés, les projets Java doivent être mis sous forme d'archives. Il existe principalement trois types d'archives, qui sont en fait des fichiers ZIP :

    1. Les **_JAR_**, pour **_Java ARchive_** : ils contiennent des classes Java et des métadonnées. Ils permettent de déployer des projets Java classiques ou des projets _EJB_.
    2. Les **_WAR_**, pour **_Web application ARchive_** : ils contiennent des classes Java, des métadonnées, et tout le contenu Web (HTML, CSS, JavaScript, images, ...). Ils permettent de déployer des projets _Web_.
    3. Les **_EAR_**, pour **_Entreprise Application aRchive_** : ils contiennent plusieurs modules (au moins un). Il peut y avoir un ou plusieurs modules _Web_, un ou plusieurs modules _EJB_ et un ou plusieurs modules Java.

Dans le menu _Entreprise Explorer_,

1. Faire un clic droit, puis :material-mouse:`New > Project...`, puis :material-file-tree:`Java EE > Entreprise Application Project`, puis <bouton><u>N</u>ext ></bouton>.
2. Nommer le projet **_GestionNotesEAR_**, vérifier la _runtime_ utilisée, puis cliquer sur <bouton><u>N</u>ext ></bouton> :

    <center>
        ![Création projet EAR](../images/GestionNotesEAR.png)
    </center>

3. Sélectionner les projets **_GestionNotesEJB_** et **_GestionNotesWeb_**, puis cliquer sur <bouton><u>F</u>inish</bouton>.


## 2. Déploiement de l'application d'entreprise

Il faut maintenant déployer l'_EAR_. Pour cela, faire un clic droit sur le serveur, :material-mouse:`Add and Remove...`. Supprimer le projet **_GestionNotesEJB_**, et ajouter le projet **_GestionNotesEAR_**, puis cliquer sur <bouton><u>F</u>inish</bouton>.

Dans les logs du serveur, vérifier que l'_EJB_ a bien été déployé, en vérifiant la présence de la ligne suivante dans les logs :

``` title="📋 Logs du serveur WildFly"
ejb:GestionNotesEAR/GestionNotesEJB/NotesBusinessImpl!fr.univtours.polytech.gestionnotes.business.NotesBusinessRemote
```

On constate au passage que le nom JNDI a légèrement changé, puisqu'on voit maintenant apparaître le nom du projet _EAR_.


## 3. Test de l'application

L'application est maintenant disponible ici [http://localhost:8080/GestionNotesWeb/notesList](http://localhost:8080/GestionNotesWeb/notesList).

???+ success "URL pour accéder à l'application"

    1. On constate que même lorsqu'on déploie l'EAR, l'URL pour accéder à l'application a toujours pour racine le `context-root` du module Web (par défaut le nom de ce projet), ici **_GestionNotesWeb_**.
    2. On peut se demander quel est l'intérêt de générer le fichier 📄`web.xml` alors que nous déployons maintenant les _Servlets_ via les annotations. Le descripteur de déploiement permet d'indiquer la ressource à exécuter par défaut. Il faut pour cela ajouter la balise `welcome-file` à l'intérieur de la balise `welcome-file-list`. Par exemple :

        ``` xml
        <?xml version="1.0" encoding="UTF-8"?>
        <web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns="http://xmlns.jcp.org/xml/ns/javaee"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
            id="WebApp_ID" version="4.0">
            <display-name>GestionNotesWeb</display-name>
            <welcome-file-list>
                <welcome-file>notesList</welcome-file>
            </welcome-file-list>
        </web-app>
        ```

        Si on exécute la requête [http://localhost:8080/GestionNotesWeb](http://localhost:8080/GestionNotesWeb), alors c'est [notesList](notesList) qui est exécutée, c'est-à-dire [http://localhost:8080/GestionNotesWeb/notesList](http://localhost:8080/GestionNotesWeb/notesList).


## 4. Retour sur le client Java

Si on exécute maintenant le client Java, on obtient dans les logs une erreur de ce genre :

``` title="📋 Logs du serveur WildFly"
Exception in thread "main" org.jboss.ejb.client.RequestSendFailedException: EJBCLIENT000409: No more destinations are available
...
...
	Suppressed: javax.ejb.NoSuchEJBException: No such EJB: /GestionNotesEJB/NotesBusinessImpl @ remote+http://localhost:8080
    ...
    ...
```

En effet, le module _EJB_ est maintenant déployé dans une application d'entreprise, et non plus directement. Il faut donc mettre à jour l'URL pour y accéder.

Pour cela, il faut modifier la classe `NoteClient`, et plus précisément la méthode `getBusiness`. Il faut maintenant indiquer le nom du projet d'entreprise :

``` java
String appName = "GestionNotesEAR";
```

Ça y est, notre SI respecte le schéma présenté au début.
