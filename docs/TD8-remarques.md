# Remarques sur le TP noté.


## Groupe 1

1 projet (3 projets fournis, mais non fonctionnel)

### Commentaire sur la partie technique

- Attention à bien formatter le code.
- Dans les Servlets, il y a des Warnings :
    - Il y a des dépendances vers la couche métier qui ne sont en fait pas utilisées.
    - Il ne faut pas récupérer l'utilisation en session s'il n'est pas utilisé ensuite.

### Commentaire sur la partie fonctionnelle

- Module de recherche : OK.
    - Dans ce module, il serait bien que les informations saisies ne soit pas effacées à chaque fois qu'on relance la recherche.
- Lorsqu'on créé un utilisateur, le champ `Username` n'est pas récupéré. Le *login* de l'utilisateur est son nom.
    Attention à bien *mapper* les champs du formulaires vers les bonnes colonnes en base de données.
- Est-ce précisé dans la doc que l'utilisateur de test est `a/a` ?
- Il n'y a aucun message d'erreur si on essaie de se connecter avec un utilisateur qui n'existe pas.
- L'historique des réservations n'est pas conservé.
- A chaque recherche, tous les critères saisis sont perdus (lorsqu'on clique sur "rechercher").

**Bugs** :
- Une fois qu'un utilisateur a emprunté un livre, même lorsqu'il l'a rendu, il apparaît toujours dans la liste des livres emprunté.
- De même, les résultats de la recherche ne sont pas à jour (les livres sont toujours indisponibles).
    => Il faut se déconnecter et se reconnecter pour que la liste soit mise à jour.
    => Le problème vient du fait que le DAO n'est pas un singleton.
- La liste des livres empruntés n'est pas correctement mise à jour. Plus préciséement, la liste n'est pas récupérée en BDD à chaque fois, ce qui peut provoquer des erreurs (erreur que je n'arrive plus à reproduire).

**Problèmes techniques** :
- Dans toutes les servlets, il y a des dépendances inutiles (vers la couche métier).
- Dans `HomeServlet`, variable `utilisateur` inutile.
- Ne pas ajouter de méthodes qui ne sont pas implémentées (`GenreDAOImplJPA.updateGenre` ou `UtilisateurDAOImplJPA.updateUtilisateur`) par exemple.
- Nommage de la variable `u_Business` dans `SignupServlet`.
- De manière générale, les règles de gestion (emprunt ou rendu d'un livre et emprunt de 5 livres max) doivent être dans la partie métier, pas dans la partie présentation.

✅
❌

| RG| Implémentation | 
|--|:--:|
| Recherche par titre | ✅ |
| Recherche par auteur | ✅ |
| Recherche par genre | ✅ |
| Recherche par disponibilité | ✅ |
| Recherche avec plusieurs critères | ✅ |
| **Bonus** Image des livres | ❌ |
| Connexion | ✅ |
| **Bonus** Mot de passe chiffré en BDD | ❌ |
| Emprunt de 5 livres au max | ✅ |
| Emprunt pour 10 jours | ✅ |
| Affichage de la liste des documents empruntés | ✅ |
| Historique des emprunts | ❌ |
| Rendu d'un livre | ✅ |
| Alerte si moins d'une semaine | ❌ |
| Alerte si en retard | ❌ |
| Partie admin pour indiquer *rendu* | ✅ |
| **Bonus** Création d'un utilisateur | Bugs |
| **Bonus** Ajout d'un livre dans la bibliothèque | ❌ |


| Point | |
|---|---|
| Respect du camelCase | |
| Pas d'import inutiles | |

| Respect du MVC |  |


___

## Groupe 2

### Commentaire sur la partie technique

3 projets.

- Il ne faut pas mettre de `persistence.xml` dans le projet Web.
- code très propre.
- Les règles de gestion doivent être implémentées dans la partie métier, pas dans le contrôleur.

### Commentaire sur la partie fonctionnelle

- Un utilisateur ne peut pas savoir les livres qu'il a emprunté (ni quand est-ce qu'il doit les rendre).
- Il n'est pas possible d'avoir l'historique des emprunts.

✅
❌

| RG| Implémentation | 
|--|:--:|
| Recherche par titre | ✅ |
| Recherche par auteur | ✅ |
| Recherche par genre | ✅ |
| Recherche par disponibilité | ✅ |
| Recherche avec plusieurs critères | ✅ |
| **Bonus** Image des livres | En partie (tous les livres ont la même image) |
| Connexion | ✅ |
| **Bonus** Mot de passe chiffré en BDD | ❌ |
| Emprunt de 5 livres au max | ✅ |
| Emprunt pour 10 jours | ❌ |
| Affichage de la liste des documents empruntés | Que pour les admin |
| Historique des emprunts | ❌ |
| Rendu d'un livre | ✅ |
| Alerte si moins d'une semaine | ❌ |
| Alerte si en retard | ❌ |
| Partie admin pour indiquer *rendu* | ✅ |
| **Bonus** Création d'un utilisateur | ❌ |
| **Bonus** Ajout d'un livre dans la bibliothèque | ❌ |



___

## Groupe 3

### Commentaire sur la partie technique

3 projets.

- Les règles de gestion doivent être implémentées dans la partie métier, pas dans le contrôleur.



### Commentaire sur la partie fonctionnelle

- Lorsqu'on clique sur "Rechercher", il faut bien que tous les champs soient conservés : *genre* est perdu à chaque fois.


✅
❌

| RG| Implémentation | 
|--|:--:|
| Recherche par titre | ✅ |
| Recherche par auteur | ✅ |
| Recherche par genre | ✅ |
| Recherche par disponibilité | ✅ |
| Recherche avec plusieurs critères | ✅ |
| **Bonus** Image des livres | ❌ |
| Connexion | ✅ |
| **Bonus** Mot de passe chiffré en BDD | ❌ |
| Emprunt de 5 livres au max | ✅ |
| Emprunt pour 10 jours | ✅ |
| Affichage de la liste des documents empruntés | ✅ |
| Historique des emprunts | ❌ |
| Rendu d'un livre | ✅ |
| Alerte si moins d'une semaine | ✅ |
| Alerte si en retard | ❌ |
| Partie admin pour indiquer *rendu* | ✅ |
| **Bonus** Création d'un utilisateur | ❌ |
| **Bonus** Ajout d'un livre dans la bibliothèque | ❌ |


___

## Groupe 4

### Commentaire sur la partie technique

3 projets.

- Les règles de gestion doivent être implémentées dans la partie métier, pas dans le contrôleur.
- Il y a de nombreux imports inutilisés, par exemple dans `BorrowBean`, `BorrowsBusinessImpl`, `BorrowsBusinessLocal`, `BorrowsBusinessRemote` et  `BorrowsImplDAO`.

### Commentaire sur la partie fonctionnelle

- Il n'y a pas assez de livres dans le jeu de tests. Il est important de faire des jeux de tests complets.
- Lorsqu'on clique sur "Rechercher", il faut bien que tous les champs soient conservés : *genre* et *available* sont perdus à chaque fois.
- Il n'est pas possible d'avoir l'historique des emprunts.


✅
❌

| RG| Implémentation | 
|--|:--:|
| Recherche par titre | ✅ |
| Recherche par auteur | ✅ |
| Recherche par genre |  ✅|
| Recherche par disponibilité | ❌ |
| Recherche avec plusieurs critères | ✅ |
| **Bonus** Image des livres | ❌ |
| Connexion | ✅ |
| **Bonus** Mot de passe chiffré en BDD | ❌ |
| Emprunt de 5 livres au max | ✅ |
| Emprunt pour 10 jours | ✅ |
| Affichage de la liste des documents empruntés | ✅ |
| Historique des emprunts | ❌ |
| Rendu d'un livre | ✅ |
| Alerte si moins d'une semaine | ❌ |
| Alerte si en retard | ✅ |
| Partie admin pour indiquer *rendu* | ✅ |
| **Bonus** Création d'un utilisateur | ❌ |
| **Bonus** Ajout d'un livre dans la bibliothèque | ❌ |


___

## Groupe 5

### Commentaire sur la partie technique

1 projet.

- Bugs sur l'emprunt : ne fonctionne pas.
- Le nommage des packages n'est pas correct.
- Il y a de nombreux imports inutilisés, par exemple dans `AuthorBean`, `BookBean`, `GenreBean`, `UserBean`, `ListBookServlet` et  `LoginServlet`.


### Commentaire sur la partie fonctionnelle

- Lorsqu'on clique sur "Rechercher", il faut bien que tous les champs soient conservés : *genre* et *available* sont perdus à chaque fois.


✅
❌

| RG| Implémentation | 
|--|:--:|
| Recherche par titre | ✅ |
| Recherche par auteur | ✅ |
| Recherche par genre | ✅ |
| Recherche par disponibilité | ✅ |
| Recherche avec plusieurs critères | ✅ |
| **Bonus** Image des livres | ❌ |
| Connexion | ✅ |
| **Bonus** Mot de passe chiffré en BDD | ❌ |
| Emprunt de 5 livres au max | ❌ |
| Emprunt pour 10 jours | ❌ |
| Affichage de la liste des documents empruntés | ❌ |
| Historique des emprunts | ❌ |
| Rendu d'un livre | ❌ |
| Alerte si moins d'une semaine | ❌ |
| Alerte si en retard | ❌ |
| Partie admin pour indiquer *rendu* | ❌ |
| **Bonus** Création d'un utilisateur | ✅ |
| **Bonus** Ajout d'un livre dans la bibliothèque | ❌ |


___
___

| RG | Groupe 1 | Groupe 2 | Groupe 3 | Groupe 4 | Groupe 5 |
|--|:--:|:--:|:--:|:--:|:--:|
| Recherche par titre | ✅ | ✅ | ✅ | ✅ | ✅ |
| Recherche par auteur | ✅ | ✅ | ✅ | ✅ | ✅ |
| Recherche par genre | ✅ | ✅ | ✅ | ✅ | ✅ |
| Recherche par disponibilité | ✅ | ✅ | ✅ | ❌ | ✅ |
| Recherche avec plusieurs critères | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Bonus** Image des livres | ❌ | En partie (tous les livres ont la même image) | ❌ | ❌ | ❌ |
| Connexion | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Bonus** Mot de passe chiffré en BDD | ❌ | ❌ | ❌ | ❌ | ❌ |
| Emprunt de 5 livres au max | ✅ |✅ | ✅ | ✅ | ❌ |
| Emprunt pour 10 jours | ✅ | ❌ | ✅ | ✅ | ❌ |
| Affichage de la liste des documents empruntés | ✅ | Que pour les admins | ✅ | ✅ | ❌ |
| Historique des emprunts | ❌ | ❌ | ❌ | ❌ | ❌ |
| Rendu d'un livre | ✅ | ✅ | ✅ | ✅ | ❌ |
| Alerte si moins d'une semaine | ❌ | ❌ | ✅ | ❌ | ❌ |
| Alerte si en retard | ❌ | ❌ | ❌ | ✅ | ❌ |
| Partie admin pour indiquer *rendu* | ✅ | ✅ | ✅ | ✅ | ❌ |
| **Bonus** Création d'un utilisateur | Bugs | ❌ | ❌ | ❌ | ✅ |
| **Bonus** Ajout d'un livre dans la bibliothèque | ❌ | ❌ | ❌ | ❌ | ❌ |
| Note | 13 | 15 | 18 | 16 | 8 |
