2021-2022

- [X] Couper le TD4 en deux TD : TD4 et TD5.
- [X] Parler de partie traitement pour ne pas confondre avec la couche métier.
- [X] Revoir les exemples GestionNotes pour que les services métiers et DOA ne soit pas exactement les mêmes.
- [X] Revoir la DAO manuel pour que toutes les erreurs soient au même niveau (sinon on ne sait pas d'où elles viennent)

- [X] Dans le TD adéquat, être plus explicite sur l'utilisation de la JSTL :
    - [X] Ajouter quelque part
        ```JSP title=""
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        ```
    - [X] Indiquer qu'il faut ajouter les JAR dans WEB-INF/lib.
 
 
 ---------------------------
 2022-2023
 
 - [X] TD1, TD2, TD3, TD4 et TD5 OK.
 - [ ] TD6 : Un peu long
 - [ ] TD6 : Utiliser l'entitymanager avec des annotations. Sinon, ça ne marche pas bien avec le merge.
    => En fait si ! Il faut mettre un singleton.

- [ ] Avant de faire le 1er TP noté, faire un résumé du cours (notamment les annotations à utiliser) pour avoir une archi correcte et fonctionnelle.

- [ ] TD9 : Prévoir deux TPs complets (et c'est même dur de finir).
- [ ] TD10 : A faire sur 2 TPs.

Pour le TD8 noté, donner le DAO.
