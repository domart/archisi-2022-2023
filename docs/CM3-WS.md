# CM WS

```java
@GET
@Path("books/{id}")
@Produces(MediaType.TEXT_PLAIN)
public String getTextBook(@PathParam("id") String id) {
    return id + " est un super livre !";
}

@GET
@Path("books/{id}")
@Produces(MediaType.APPLICATION_JSON)
public String getJsonBook(@PathParam("id") String id) {
    return "{'id': " + id + ", 'comment': 'Super livre !!'}";
}

@GET
@Path("books/{id}")
@Produces(MediaType.TEXT_HTML)
public String getHtmlBook(@PathParam("id") String id) {
    return "<html><body>" + id + " est un super livre !</body></html>";
}
```