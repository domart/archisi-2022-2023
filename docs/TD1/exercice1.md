---
author: Benoît Domart
title: Exercice 1 - L'environnement de développement
tags:
  - 0-simple
---

# Exercice 1 - L'environnement de développement

## 1. Installation de l'environnement de développement

Première chose à faire avant de se lancer dans le développement : installer l'environnement de développement. Pour cela, crée quelque part sur ton poste un dossier 📂`JEE` dans lequel nous allons placer tous les outils nécessaires au developpement de notre projet. Pour cette première version de notre site, nous allons avoir besoin de trois choses.

???+success

    À la fin, ton dossier 📂`JEE` devra ressembler à ça :
    <pre style="line-height: 1.2;">
        JEE/
        ├── eclipse/
        ├── JDK/
        │   └── jdk1.8.0_xyz/
        └── serveurs/
            └── apache-tomcat-9.0.44/
    </pre>

1. La dernière version d'**eclipse**.

    On la trouve ici : [https://www.eclipse.org/downloads/](https://www.eclipse.org/downloads/){target=_blank}. Attention, il faut bien sélectionner "**Eclipse IDE for Entreprise Java and Web Developers**" comme dans la capture d'écran ci-dessous (sinon tu ne pourras pas créer de projet JEE) :
    <center>![Installation d'Eclipse](../images/install_eclipse.png)</center>

1. La version 8 de la **jdk**.

    === "Pour Windows"
        ???+success "Pour Windows"

            Si tu utilises Windows, tu peux directement télécharger la JDK ici :

            <center>
                [jdk-8u351-windows-x64.exe](../JDK/jdk-8u351-windows-x64.exe){ .md-button download="jdk-8u351-windows-x64.exe" }
            </center>

    === "Pour un autre OS (ou pour une autre sous-version)"
        ???+success "Pour un autre OS (ou pour une autre sous-version)"
        
            On la trouve ici : [https://www.oracle.com/java/technologies/downloads/#java8](https://www.oracle.com/java/technologies/downloads/#java8){target=_blank} (il faut un compte pour se connecter).

            ???+warning "Attention"

                Avec ce lien, attention à bien sélectionner ton système d'exploitation (c'est Linux par défaut) !
    
    ???+success "Bien configurer Java"

        Pour bien configurer Java, et savoir quelle est la version que tu utilises, voici ce qu'il faut faire :

        * Crée (ou modifie) la variable d'environnement `JAVA_HOME`, dont la valeur est le dossier où est la _JDK_.

			Pour être sûr que c'est le bon, c'est le dossier qui contient le sous-dossier 📂`bin` qui contient lui-même les exécutables ⚙️`java`, ⚙️`javaw`, ⚙️`javac` ...

            Par exemple, sous Windows, on aura quelque chose comme :

            <center>![Variable d'environnement JAVA_HOME](../images/var_env_java_home.png)</center>

        * Crée (ou modifie) la variable d'environnement `JRE_HOME`, dont la valeur est celle de `JAVA_HOME`.

            Sous Windows, il faut donc indiquer `%JAVA_HOME%` comme valeur.

            Par exemple, sous Windows, on aura quelque chose comme :

            <center>![Variable d'environnement JAVA_HOME](../images/var_env_jre_home.png)</center>
        
        * Modifie la variable `PATH` (:warning: elle existe déjà !), en ajoutant **en premier** un lien vers le dossier 📂`bin` de la JDK (celui qui contient les exécutables).

            Par exemple, sous Windows, on aura quelque chose comme :

            <center>![Variable d'environnement JAVA_HOME](../images/var_env_path.png)</center>
			
			???+warning "Attention !"
			
				Attention à ne pas oublier le `\bin` dans `%JAVA_HOME%\bin` !
            
            ???+danger "Attention !!"

                Attention à ne pas écraser ou supprimer cette variable d'environnement, cela pourrait entraîner des dysfonctionnements dans d'autres logiciels !
        
        * Enfin, ouvre un terminal et exécute la commande `java -version`

            Si tu obtiens quelque chose qui ressemble à ça :

            <center>
            ![Java Version](./java_version.gif)
            </center>

            c'est bon signe !
			
			Sinon, revois les étapes précédentes, tu as du oublier quelque chose ...

1. La version 9 de **tomcat**.

    Ici, nous n'allons pas télécharger la dernière version (la 10 actuellement), car elle implémente Jakarta EE 9. Nous allons télécharger la version tomcat9 (disponible ici : [https://tomcat.apache.org/download-90.cgi](https://tomcat.apache.org/download-90.cgi){target=_blank}), qui implémente Java EE 8. De manière générale, il peut être judicieux de ne pas utiliser les toutes dernières versions des produits, qui peuvent contenir des bugs non encore connus (de sécurité notamment) ...

    Il suffit de télécharger le zip (si on travaille avec windows), et de le dézipper dans le sous-dossier 📂`serveurs` dossier 📂`JEE`.

    ???+success "Le serveur tomcat"

        Tomcat va nous permettre de simuler un serveur, sur notre propre machine.

        Ainsi, en reprennant le schéma du cours, nous aurons le client (notre navigateur) et le serveur (tomcat) sur la même machine : notre PC.

        <center>![Architecture client-serveur](../images/archi-client-serveur.png)</center>



## 2. Configuration de l'environnement de développement

1. On commence par lancer eclipse, en indiquant comme espace de travail (_workspace_) 📂`JEE/ws_archisi`.

    ???+note "Remarque"

        On pourra placer tous les projets que nous allons créer dans ce cours dans ce _workspace_. Ils auront tous un nom différent, donc cela ne posera pas de problème.

2. Dans eclipse, on sélectionne la _JDK_ 8 comme _JRE_ (pour _Java Runtime Environment_) par défaut.
    * Pour cela, on va dans le menu :fontawesome-solid-bars:`Window > Preferences`, puis dans l'arborescance :material-file-tree:`Java > Installed JREs`.
    * On clique sur <bouton><u>A</u>dd...</bouton>, on sélectionne _Standard VM_, on clique sur <bouton>Direct<u>o</u>ry...</bouton> pour indiquer le dossier où est présente la _JDK_ 8 et enfin sur <bouton>Finish</bouton>.
    * Dans la liste des _JREs_, on coche la case correspondant à cette _JDK_ pour que ce soit celle par défaut.

    <center>
    ![Java Version](./eclipse_default_jre.gif)
    </center>
    
2. Nous allons développer une application JEE, déployée sur un serveut tomcat 9. Eclipse nous permet de définir un **environnement d'éxécution** (_**runtime environment**_ en anglais).

    Ainsi, en associant un projet donné à un _runtime_ donné, toutes les bibliothèques nécessaires sont automatiques ajoutées au projet. **Il n'y aura pas besoin d'importer manuellement tous les _JARs_ nécéssaires.**

    * Pour cela, dans Eclipse, on va dans le menu :fontawesome-solid-bars:`Window > Preferences`, puis dans l'arborescance :material-file-tree:`Server > Runtime Environments`, puis <bouton><u>A</u>dd...</bouton>.
    * Dans la liste, sélectionner :material-file-tree:`Apache > Apache Tomcat v9.0` puis <bouton><u>N</u>ext ></bouton> (décocher la case `Create a new local server` si elle est cochée).
    * Cliquer sur <bouton>Browse...</bouton> pour indiquer le dosser où tomcat a été dézippé.
    * On peut enfin cliquer sur <bouton>Finish</bouton>.

    <center>
    ![Java Version](./eclipse_apache_runtime.gif)
    </center>