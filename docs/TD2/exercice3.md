---
author: Benoît Domart
title: Exercice 3 - Connexion
tags:
  - 0-simple
---

# Exercice 3 - Page de connexion

Reprends le code de l'exercice précédent, et modifie-le pour que l'utilisateur soit redirigé vers :

- une page HTML affichant "Salut !", si le mot de passe saisi est `password`,
- une page HTML affichant à nouveau le formulaire, ainsi qu'un message d'erreur en rouge indiquant :

    <span style="color:red">Nom d'utilisateur ou mot de passe érronné</span>
    
    si le mot de passe saisi n'est pas `password`.

<center>
    ![Exemples](./exemple_connexion.gif)
</center>